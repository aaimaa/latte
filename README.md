Full-stack web application framework that uses Python and MariaDB on the server side and a tightly integrated client side library.

### Table of Contents
* [Installation](#installation)
* [License](#license)


### License
This repository has been released under the [MIT License](LICENSE).
